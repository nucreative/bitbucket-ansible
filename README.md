BITBUCKET ANSIBLE SCRIPT
========================

General
-------

This is an open source project that will install Bitbucket Server on to your Ubuntu Server. Please note, you will need your own license to use this software and we do not provide one. If you don't currently have one, you can get yours from here: [Bitbucket Server License](https://www.atlassian.com/software/bitbucket/pricing?tab=host-on-your-server)

This project is designed to be used if you want to easily replicate your Bitbucket installation on multiple servers or you want a easy to use script just in case something goes wrong with your server and need to quickly install the Bitbucket instance again.

Installation
------------

Once you clone/download this repository to your local machine you only need to change a few settings and you will be ready to start your very own Bitbucket Server.

  1. To begin with you will need to change the variables in both the sensitive.yml and variables.yml files, these can be found in "/roles/vars/" directory, these files contain all the variables you will need to launch your server. As you can probably guess from the names one is for sensitive information like usernames and passwords for your Database and Bitbucket install, the other is for less sensitive information. If you want though you can move all the variables in to one of the other.

  2. Once you have correctly replaced all of the [placeholder] vars you will want to encrypt at least the sensitive.yml file. You can achieve this by navigating to the root directory in terminal and using the following command `ansible-vault encrypt /roles/vars/sensitive.yml` you will then be asked to set a password so you can use and decrypt this file in the future.

  3. If you changed the version of Bitbucket you want to use from 4.7.1 you will need to update the following lines in the following files to correspond with your new version number.
    * `/roles/bitbucket/tasks/mail.yml` Line Number: `26`
    * `/roles/init_d_script/templates/init.d_bitbucket.tpl` Line Number: `19`
    * `/roles/vars/variables.yml` Line Number: `2`

  4. Next you will need to update your email settings so your Bitbucket Server can send emails to users via Postfix. These will be the username and password you will need to input in to Bitbucket Settings to login to Postfix. The file you need to change is `roles/server/templates/virtual`.

  The format you need to follow when creating your username and password in the `virtual` file is:
    * `[username]   [password]` the space between username and password should be a double tab. Or 6 individual spaces, whichever you prefer.

  5. Now that you have set all your install variables you will need to declare your remote server in the `inventory` file. There is an example of how this should look in the file but if you want to deploy to more than one server at a time and use groups take a look at the [Ansible Documentation for this](http://docs.ansible.com/ansible/intro_inventory.html).
