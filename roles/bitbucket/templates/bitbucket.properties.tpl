setup.displayName=NU Creative Bitbucket

setup.baseUrl={{ bitbucket_URL }}

setup.license={{ license }}

setup.sysadmin.username={{ bitbucket_username }}

setup.sysadmin.password={{ bitbucket_password }}

setup.sysadmin.displayName={{ bitbucket_display_name }}

setup.sysadmin.emailAddress={{ bitbucket_email }}

jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/{{ db_database_name }}?characterEncoding=utf8&useUnicode=true&sessionVariables$
jdbc.user={{ db_username }}
jdbc.password={{ db_password }}
